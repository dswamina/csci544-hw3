Please Enjoy
the Following Sample
•	 This sample is an excerpt from a Samuel French
title.
•	 This sample is for perusal only and may not be
used for performance purposes.
•	 You may not download, print, or distribute this
excerpt.
•	 We highly recommend purchasing a copy of
the title before considering for
performance.

For more information about
licensing or purchasing a play or
musical, please visit our websites

www.samuelfrench.com
www.samuelfrench-london.co.uk

A Few Good Men
by Aaron Sorkin

A Samuel French Acting Edition

samuelfrench.com

Copyright © 1990, 1998, 2012 by Aaron Sorkin
ALL RIGHTS RESERVED
Cover Art © 1990 Serino Coyne, Inc.
CAUTION: Professionals and amateurs are hereby warned that A FEW
GOOD MEN is subject to a licensing fee. It is fully protected under the
copyright laws of the United States of America, the British Commonwealth, including Canada, and all other countries of the Copyright
Union. All rights, including professional, amateur, motion picture, recitation, lecturing, public reading, radio broadcasting, television and the
rights of translation into foreign languages are strictly reserved. In its
present form the play is dedicated to the reading public only.
The amateur and professional live stage performance rights to A
FEW GOOD MEN are controlled exclusively by Samuel French, Inc., and
licensing arrangements and performance licenses must be secured well
in advance of presentation. PLEASE NOTE that amateur licensing fees
are set upon application in accordance with your producing circumstances. When applying for a licensing quotation and a performance
license please give us the number of performances intended, dates of
production, your seating capacity and admission fee. Licensing fees
are payable one week before the opening performance of the play to
Samuel French, Inc., at 45 W. 25th Street, New York, NY 10010.
Licensing fee of the required amount must be paid whether the play
is presented for charity or gain and whether or not admission is charged.
Professional/Stock licensing fees quoted upon application to Samuel
French, Inc.
For all other rights than those stipulated above, apply to: Agency
for Performing Arts, c/o Steve Fisher, 405 South Beverly Drive, Beverly
Hills, CA 90212.
Particular emphasis is laid on the question of amateur or professional
readings, permission and terms for which must be secured in writing
from Samuel French, Inc.
Copying from this book in whole or in part is strictly forbidden by
law, and the right of performance is not transferable.
Whenever the play is produced the following notice must appear on
all programs, printing and advertising for the play: “Produced by special
arrangement with Samuel French, Inc.”
Due authorship credit must be given on all programs, printing and
advertising for the play.

ISBN 978-0-573-70051-4

Printed in U.S.A.

#8146

No one shall commit or authorize any act or omission by which the
copyright of, or the right to copyright, this play may be impaired.
No one shall make any changes in this play for the purpose of
production.
Publication of this play does not imply availability for performance.
Both amateurs and professionals considering a production are
strongly advised in their own interests to apply to Samuel French,
Inc., for written permission before starting rehearsals, advertising,
or booking a theatre.
No part of this book may be reproduced, stored in a retrieval system, or transmitted in any form, by any means, now known or yet
to be invented, including mechanical, electronic, photocopying,
recording, videotaping, or otherwise, without the prior written permission of the publisher.
MUSIC USE NOTE
Licensees are solely responsible for obtaining formal written permission
from copyright owners to use copyrighted music in the performance of this
play and are strongly cautioned to do so. If no such permission is obtained
by the licensee, then the licensee must use only original music that the
licensee owns and controls. Licensees are solely responsible and liable for
all music clearances and shall indemnify the copyright owners of the play
and their licensing agent, Samuel French, Inc., against any costs, expenses,
losses and liabilities arising from the use of music by licensees.
The various military chants included in A FEW GOOD MEN, whose use is
both necessary and mandatory in productions, are under copyright protection. MUSIC ROYALTY IS AS FOLLOWS:
Amateur Productions: $2.50 per performance, all-inclusive.
Stock Productions: Terms quoted on application,

IMPORTANT BILLING AND CREDIT
REQUIREMENTS
All producers of A FEW GOOD MEN must give credit to the Author of the
Play in all programs distributed in connection with performances of the
Play, and in all instances in which the title of the Play appears for the purposes of advertising, publicizing or otherwise exploiting the Play and/or
a production. The name of the Author must appear on a separate line on
which no other name appears, immediately following the title and must
appear in size of type not less than fifty percent of the size of the title type.
In addition the following credit must be given in all programs and publicity information distributed in association with this piece:
Broadway production presented by David Brown, Lewis Allen,
Robert Whitehead, Roger L. Stevens, Kathy Levin,
Suntory International Corporation, and The Shubert Organization

A FEW GOOD MEN was originally presented at the Heritage Repertory Theatre of the Univeristy of Virginia, Department of Drama, and
subsequently, in association with the John F. Kennedy Center for the Performing Arts, was presented at the Music Box Theatre in New York, on
November 15, 1989, under the direction of Don Scardino, with Dianne
Trulock as production stage manager, and with designs by: Ben Edwards,
set; David C. Woolard, costume; Thomas R. Skelton, light; and John Gromada, sound; and the cast was as follows:
SENTRY . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . Ron Ostrow
HAROLD DAWSON . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . Victor Love
LOUDEN DOWNEY . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . Michael Dolan
SAM WEINBURG . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . Mark Nelson
DANIEL A. KAFFEE . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . Tom Hulce
JOANNE GALLOWAY . . . . . . . . . . . . . . . . . . . . . . . . . . . . Megan Gallagher
ISAAC WHITAKER . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . Edmond Genest
MATTHEW A. MARKINSON . . . . . . . . . . . . . . . . . . . . . . . . . . Robert Hogan
WILLIAM T. SANTIAGO . . . . . . . . . . . . . . . . . . . . . . . . . . . . Arnold Molina
NATHAN JESSEP . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . Stephen Lang
JONATHAN JAMES KENDRICK. . . . . . . . . . . . . . . . . . . . . . . . .Ted Marcoux
JACK ROSS . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .Clark Gregg
JEFFREY OWEN HOWARD . . . . . . . . . . . . . . . . . . . . . . . . . Geoffrey Nauffts
JULIUS ALEXANDER RANDOLPH . . . . . . . . . . . . . . . . . . . . . . . Paul Butler
WALTER STONE . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . Fritz Sperberg
MARINES, SAILORS, M.P.’S, LAYWERS, et al. . . . Stephen Bradbury, Jeffrey

Dreisbach

CHARACTERS
LANCE CPL. HAROLD W. DAWSON
PFC LOUDEN DOWNEY
LT. J.G. SAM WEINBERG
LY. J.G. DANIEL A. KAFFEE
LT. CMDR. JOANNE GALLOWAY
CAPT. ISAAC WHITAKER
CAPT. MATTHEW A. MARKINSON
PFC. WILLIAM T. SANTIAGO
LT. COL. NATHAN JESSEP
LT. JONATHAN JAMES KENDRICK
LT. JACK ROSS
CPL. JEFFREY OWEN HOWARD
CAPT. JULIUS ALEXANDER RANDOLPH
CMDR. WALTER STONE
MARINES, SAILORS, M.P.’S, LAYWERS, et al.

SCENE
The action takes place in various locations in Washington, D.C., and
on the United States Naval Base in Guantanomo Bay, Cuba.

TIME
Summer, 1986

DAWSON. I, Lance Corporal Harold W. Dawson, have

been informed by Special Agent R.C. McGuire of the
Naval Investigative Service, that I am suspected of
Murder, Conspiracy to Commit Murder, and Conduct
Unbecoming a United States Marine in the matter of
Private First Class William T. Santiago. I have also been
advised that I have the right to remain silent and make
no statement at all.
DOWNEY. Any statement I do make can be used against me
in a trial by court-martial or other judicial or administrative proceeding. I have the right to consult with a
lawyer prior to further questioning.
DAWSON. I am presently assigned to Rifle Security Company
Windward, Second Platoon Delta, NAVBASE, Guantanamo Bay, Cuba.
DOWNEY. I am a PFC in the United States Marine Corps
assigned to Marine Rifle Security Company Windward,
Second Platoon Delta. I will have been in the Marine
Corps ten months as of August.
DAWSON. I entered Private Santiago’s barracks room on the
evening of 6 July, at or about 23:50. I was accompanied
by PFC Louden Downey.
DOWNEY. I was accompanied by my squad leader, Lance
Corporal Harold W. Dawson.
DAWSON. We tied his hands and feet with rope.
DOWNEY. We tied Private Santiago’s hands and feet with
rope and we forced a piece of cloth into his mouth.
DAWSON. We placed duct tape over his eyes and mouth.
DOWNEY. I have read this two page statement that Special
Agent McGuire has prepared for me at my request, as
we discussed its content. I have been allowed to make
all changes and corrections, initializing those changes
and corrections.
7

8

A FEW GOOD MEN

DAWSON. These statements are true and factual to the best

of my knowledge.
(Lights up on KAFFEE’s office.)
(SAM is entering. KAFFEE’s in a hurry.)
SAM. Danny –
KAFFEE. I’m late.
SAM. You know what I just saw?
KAFFEE. No, but I’m genuinely late.
SAM. There’s a lady lawyer from internal affairs wandering

around the hallway.
KAFFEE. What’s she doing?
SAM. I don’t know.
KAFFEE. Is she stealing things?
SAM. No.
KAFFEE. Tell me why I care.
SAM. Ordinarily, when internal affairs sends a lawyer around

to talk to the lawyers, it means someone’s screwed up.
KAFFEE. Do you think it’s you?
SAM. No.
KAFFEE. Have you done anything wrong?
SAM. No.
KAFFEE. You sure?
SAM. Yes I’m sure. I think so. I don’t know, I’ve been very
tired lately. Look, do me a favor, would you?
KAFFEE. Sure.
SAM. If she talks to you, if she mentions anything about
DeMattis –
KAFFEE. Who?
SAM. DeMattis. The engineer. Remember, my guy who was
littering in the admiral’s tulip garden. I may have cut a
few corners. Would you cover me?
KAFFEE. Sure.
SAM. Yeah?

A FEW GOOD MEN

KAFFEE. I don’t know what you’re talking about, but sure,

no problem.
SAM. DeMattis. He’s an engineer –
KAFFEE. Littering in the Admiral’s turnip garden.
SAM. Tulips.
KAFFEE. Okay.
SAM. Where are you going?
KAFFEE. I’m representing an ensign who bought and
smoked ten dollars worth of oregano.
SAM. He thought it was weed?
KAFFEE. I can only hope.
SAM. You’re not concerned?
KAFFEE. What’s he gonna be charged with, possession of
a condiment? He’ll get a C Misdemeanor, 15 days
restricted duty.
SAM. I’m talking about the lady from internal affairs, you’re
not concerned?
KAFFEE. My softball team’s playing Bethesda Medical
tomorrow, I can’t be concerned with anything right
now. I’ll see you at lunch.
(KAFFEE exits as – )
(Lights up on WHITAKER’s office.)
JO. I’m Lt. Commander Joanne Galloway, sir.
WHITAKER. Captain Whitaker, come on in.
JO. I appreciate your seeing me on such short notice.
WHITAKER. Bronsky said you were reopening a case.
JO. Yes sir.
WHITAKER. Bronsky and I go way back.
JO. He speaks very highly of you, sir.
WHITAKER. Yeah, that’s bullshit, right?
JO. Yes sir.
WHITAKER. I know you, don’t I?
JO. I don’t believe we’ve formally –
WHITAKER. You work at internal affairs.

9

10

A FEW GOOD MEN

JO. Yes sir.
WHITAKER. I hate internal affairs.
JO. Yes sir.
WHITAKER. And you’re a woman.
JO. Yes sir.
WHITAKER. Well that’s all right.
JO. Thank you, sir.
WHITAKER. You were the one who recycled those 14 B

Misdemeanors last winter.
JO. That may have been me.
WHITAKER. 14 B Misdemeanors. Drunk and Disorderlies.

We had ’em closed.
JO. No sir, you didn’t. The blue copies of the charge sheets
weren’t filed to Division with the IC-1.
WHITAKER. (pause) Who gives a shit??!!
JO. My boss, the Judge Advocate General.
WHITAKER. He doesn’t care any more than I do, it was you.
JO. There are rules, sir, I’m sure you understand.
WHITAKER. You had my guys working Christmas day, filling out charge sheets in long hand. Christmas day,
Commander.
JO. It was in the interest of justice, sir.
WHITAKER. Okay, are you here to bother anybody?
JO. Absolutely not. No, sir. Not at all. Only if necessary.
WHITAKER. What can I do for you?
JO. Two prisoners are being held in Guantanamo Bay,
Cuba. They pleaded guilty to Murder 2, Conspiracy
to Commit, and Conduct Unbecoming. Over the
weekend, I petitioned Captain Bronsky to deny the
guilty pleas, and to order the prisoners moved here to
Washington to be assigned council.
WHITAKER. What was the problem with the guilty pleas?
Somebody mis-spell Conspiracy?
JO. No sir, but the prisoners confessed to murder at three
o’clock in the morning during a twenty minute interview at which neither had an attorney.

A FEW GOOD MEN

WHITAKER. So Bronsky’s bringing ’em up to Washington.
JO. You’ll be receiving a memo from Division instruct-

ing you to assign an attorney from your department.
Which brings me to why I’m here.
WHITAKER. Yes.
JO. I’d like a favor.
WHITAKER. Good luck to you.
JO. Thank you.
WHITAKER. What’s the favor?
JO. Tell Division you want to assign a lawyer outside your
department.
WHITAKER. Why?
JO. Because I’m a lawyer outside your department.
WHITAKER. And don’t think I’m not grateful.
JO. I’ve brought a letter of recommendation from Captain
Bronsky.
WHITAKER. You’re an investigator, why do you want to get
mixed up in grunt work.
JO. I don’t consider it grunt work, sir.
WHITAKER. It’s a five minute plea bargain and a week of
paperwork.
JO. I’d look forward to it with relish, sir.
WHITAKER. And can I ask, do you always talk as if your dialogue was written by someone who’s not very good at
it?
JO. I’m sorry if my over-eagerness is grating.
WHITAKER. It’s not, it’s endearing. You could have a career
as a cartoon squirrel.
JO. I want to make sure this is handled properly.
WHITAKER. Have you done litigation before?
JO. My first year with the JAG Corps.
WHITAKER. How many cases did you handle?
JO. Altogether?
WHITAKER. Yes.
JO. Six.

11

12

A FEW GOOD MEN

WHITAKER. How’d you do?
JO. From what perspective?
WHITAKER. Your client’s.
JO. Not well.
WHITAKER. Okay.

(A LAWYER enters with an inter-office memo and hands
it to WHITAKER.)
LAWYER. Excuse me, Isaac, this just came for you. It’s from

Division.
JO. Those cases were lost on their merits, sir.
WHITAKER. (to the LAWYER) This is Lt. Commander Joanne
Galloway.
LAWYER. Really?
JO. How do you do?
LAWYER. Really enjoyed last Christmas.
WHITAKER. (to the LAWYER) That’ll be all.
(The LAWYER exits. WHITAKER is looking over the
memo.)
JO. So what do you say?
WHITAKER. Commander – may I call you Joanne?
JO. Yes. Please.
WHITAKER. Joanne, you seem like a fairly harmless neurotic

person –
JO. I appreciate that.
WHITAKER. And I’d like to help you out, but there are two
things preventing me. The first is that while I sincerely
believe that in your present assignment with internal
affairs you do an exceptionally thorough job, I have a
hunch that as a litigator…you know, not so much.
JO. Yes, but –
WHITAKER. The second is that Division already chose an
attorney.
(JO’s a little shocked.)
JO. (pause) What?

A FEW GOOD MEN

WHITAKER. (showing her the memo) They’ve already assigned

someone. I’m not sure why they care, but it’s out
of my hands now. They want you to brief the man.
Apparently you’ve got some letters and documents.
JO. Yes.
WHITAKER. We have a staff meeting at three, I’ll be giving
out assignments then. Come by, do your thing, try not
to make anyone cry.
JO. Yes sir.
WHITAKER. Tough break.
JO. Thank you, Captain.
WHITAKER. You can call me Isaac.
JO. And what’s the name of the attorney?
(DAWSON and DOWNEY snap to attention in the brig.)
DAWSON. Ten-hut. Officer on deck.
WHITAKER. Daniel Kaffee.

(Lights up on brig.)
(MARKINSON enters. Quietly and with difficulty, he
addresses DAWSON and DOWNEY)
MARKINSON. They’re giving you a lawyer. They’re gonna

move you up to Washington D.C. and give you a lawyer
who’s gonna ask you some questions. I want you to
remember something about these lawyers. They don’t
care about anything. They don’t care about honor or
loyalty. They don’t care about Colonel Jessep or Lt.
Kendrick, they don’t care about me and they don’t
care about you. They’re clowns. That is why, so help
me God, they’re the only ones who can save you right
now.
(beat)
I want you boys to be smart. Talk to your lawyer.
(We hear WHITAKER speaking from the staff meeting.)
WHITAKER. I’d just settle for the O.T.H., it’s his fourth U.A.,

you’re not gonna do any better than that.

13

14

A FEW GOOD MEN

(Lights up on the staff meeting.)
LAWYER #1. I don’t think I need to settle for the O.T.H. if I

file a motion to suppress, I can –
WHITAKER. A motion to suppress?
LAWYER #1. Absolutely.
WHITAKER. On what grounds?
LAWYER #1. (pause) Grounds?
LAWYER #2. See, this is where your strategy begins to fall
apart.
WHITAKER. Take the O.T.H.
(KAFFEE enters.)
KAFFEE. Excuse me, I’m sorry I’m late.
WHITAKER. I’m sure you have a good excuse.
KAFFEE. No, I just didn’t really care enough about this

meeting to be on time.
WHITAKER. (to JO) He’s kidding. Commander Galloway, this
is Lt. Kaffee.
KAFFEE. How do you do?
JO. You’re a J.G.
KAFFEE. I beg your pardon?
JO. (to WHITAKER) This is the attorney Division assigned?
WHITAKER. Yes.
JO. I wrote a seventeen page memo to Bronsky outlining
the situation, I pleaded my case for a half hour in
his living room on a Sunday afternoon, and Division
assigned a Lt. Junior Grade?
KAFFEE. Have I come at a bad time?
WHITAKER. (to KAFFEE) Commander Galloway’s from internal affairs.
KAFFEE. Oh. Ahhh…Whatever Sam did with the guy in the
tulip garden, it wasn’t his fault, he was tired. (to SAM)
How’s that?
SAM. Thanks very much.
KAFFEE. Sam has a baby at home and he’s sure she’s about
to say her first word any day now.

A FEW GOOD MEN

WHITAKER. How do you know?
SAM. She just looks like she has something to say.
WHITAKER. She’s fourteen months old, what could she

have to say?
KAFFEE. We’ve got a pool going if you want to get in on it.

Ten bucks. Pick a word off the grid.
WHITAKER. What’s left?
KAFFEE. Rosebud.
JO. Captain, with all due respect –
WHITAKER. Let’s get started. Danny, Commander Galloway’s

here ’cause you’ve been detailed by Division.
(“Oooh’s” and “Ahhh’s” from the other LAWYERS.)
KAFFEE. Detailed to do what?
WHITAKER. Detailed to handle this.

(WHITAKER hands him some files.)
Everybody listen up: Guantanamo Bay, Cuba. A Marine
PFC named William Santiago writes a letter claiming
he knows the name of a Marine on the base who illegally fired a round from his weapon over the fenceline.
Santiago ends the letter by saying he wants a transfer
off the base in exchange for the identity of the Marine.
KAFFEE. What’s a fenceline?
WHITAKER. Sam?
SAM. A big wall separating the good guys from the bad
guys.
KAFFEE. Okay.
WHITAKER. The man who fired over the fenceline was
Santiago’s squad leader, Lance Corporal Harold
Dawson. The fenceline shooting, however, is completely beside the point.
KAFFEE. What’s the point?
WHITAKER. Santiago’s dead.
SAM. What happened?

15

16

A FEW GOOD MEN

WHITAKER. Dawson and another member of the squad,

PFC Louden Downey, went into Santiago’s room, tied
his hands and feet and stuck a rag into his mouth. The
doctor said the rag must have been treated with some
kind of toxin.
KAFFEE. They poisoned the rag?
WHITAKER. Not according to them.
KAFFEE. What do they say?
WHITAKER. Not much. They’re being brought up here
tomorrow morning. Thursday at oh-six-hundred you’ll
catch a transport down to Cuba for the day to find out
what you can. Commander Galloway’s gonna fill you
in on the rest. Any questions so far?
KAFFEE. Was that oh-six-hundred in the morning, sir?
WHITAKER. Division wants me to assign back-up. Any volunteers?
SAM. No.
WHITAKER. Sam.
SAM. Sir, I have a pile of work on my desk that –
WHITAKER. Work with Kaffee on this.
SAM. Doing what?
WHITAKER. Various administrative…you know…things.
Back up. Whatever.
SAM. In other words I have no responsibilities whatsoever.
WHITAKER. Right.
SAM. My kinda case.
JO. Lt. Kaffee, how long have you been in the Navy?
KAFFEE. I’m sorry?
JO. How long have you been in the Navy?
KAFFEE. Going on nine months now.
JO. Have you ever been in a courtroom?
KAFFEE. I once had my driver’s license suspended.
JO. Alright. Captain this is absurd –
WHITAKER. Danny. Commander, if this thing ever went to
court, those Marines wouldn’t need a lawyer, they’d
need a priest.

A FEW GOOD MEN

JO. No, they’d need a lawyer.
KAFFEE. Isaac, I’d like to say for the record that this is the

least fun I’ve ever had at one of your staff meetings.
WHITAKER. (to JO) Lt. Kaffee’s generally considered one of
the best litigators in our office. He’s successfully plea
bargained 44 cases in less than a year.
KAFFEE. One more and I get a set of steak knives.
(JO takes a large file out of her briefcase and hands it to
SAM.)
JO. One of the people you’ll be talking to down there is the

barracks C.O., Colonel Nathan Jessep, I assume you’ve
heard of him.
KAFFEE. (pause) Sam?
SAM. He’s been in the papers lately. He’s expected to be
appointed Director of Operations for the NSC. Golden
boy of the Corps. Very big inside the DOD.
KAFFEE. How does somebody get very big inside the DOD?
Is he touring, did he cut an album?
(JO hands KAFFEE a stack of letters.)
JO. On top is an inventory of Santiago’s foot locker on the

night he died. Four pairs of camouflage pants, three
long sleeve khaki shirts, three short sleeve khaki shirts,
three pairs of boots, four pairs of green socks, four
pairs of black socks –
KAFFEE. Commander?
JO. Yes.
KAFFEE. I’m not sure that socks and underwear are gonna
figure too heavily into this defense.
JO. I’m saying we need to get his personal belongings to
his family after they’ve cleared evidence.
KAFFEE. Sam, you’re in charge of socks and underwear.
SAM. So it’s a good thing I went to school for 21 years.
JO. (ignoring) These are letters that Santiago wrote in his
eight months at GITMO –
SAM. (to KAFFEE) Guantanamo Bay.

17

18

A FEW GOOD MEN

KAFFEE. I knew that one.
JO. He wrote to his recruiter, HQ Atlantic, the Commandant

of the Marine Corps, even his senator. He wanted to
be transferred off the base. Nobody was listening. You
with me?
KAFFEE. Yes.
JO. Finally he wrote this letter –
(She hands it to KAFFEE, who hands it to SAM.)
– where he offered information about Corporal
Dawson’s fenceline shooting in exchange for a transfer.
This letter is the only physical evidence establishing a
motive for Dawson to kill Santiago.
KAFFEE. Gotcha.
(beat)
And Santiago is who?
JO. (beat) The victim.
KAFFEE. (to SAM) Write that down. (to JO) Am I right in

assuming these letters don’t paint a flattering picture
of Santiago’s treatment by the Marine Corps?
JO. Yes, among other–
KAFFEE. And am I also right in assuming that a protracted
investigation of the incident might cause some embarrassment for Sinatra?
JO. Who?
KAFFEE. The Base Commander, the guy who’s hot at the
Pentagon.
JO. Colonel Jessep, yes, but the point –
KAFFEE. Twelve years.
JO. I’m sorry?
KAFFEE. I’ll get it knocked down to Involuntary Manslaughter.
Twelve years. They’ll probably be home in seven.
JO. You haven’t talked to a witness or looked at a piece of
paper.
KAFFEE. Pretty impressive.
JO. Either that or criminally stupid. Which do you guess
I’m thinking it is right now?

A FEW GOOD MEN

WHITAKER. Kids –
KAFFEE. Excuse me, sir. Ma’am, do you have some sort of

jurisdiction here that I should know about?
JO. I’m special counsel for internal affairs, Lieutenant, my
jurisdiction’s pretty much in your face. Read the letters. (to WHITAKER) Thank you for the time, Captain.
WHITAKER. You’re not leaving already, are you?
JO. Yes sir. I need to audit the paper work on an engineer
who was found littering in the admiral’s tulip garden.
Someone may have forgotten to dot a few “i”’s.
(JO exits.)
KAFFEE. Hey, Sam, I think she was talkin’ about you.
SAM. You think?
WHITAKER. The two of you, don’t get cute down there. The

Marines in Guantanamo are fanatical.
KAFFEE. About what?
SANTIAGO. Dear Sir,
WHITAKER. About being Marines.

(Lights up on SANTIAGO.)
SANTIAGO. My name is PFC William T. Santiago. I am

a Marine stationed at Marine Barracks, Windward,
Guantanamo Bay, Cuba. I am writing to inform you
of my problems and to ask for your help. I have been
mistreated since the very first day I arrived. I’ve been
punished for passing out on runs when the doctor says
I just have heat exhaustion. This is just one incident
of mistreatment and I could say many more but I do
not want to take more of your time than I am allowed
to. I’ve written many letters and gotten no response
back so I must try something else. I know of an illegal fenceline shooting that took place four nights ago.
A member of my unit illegally discharged his weapon
into Cuban territory. I will give his name in exchange
for a transfer. I ask you to help me. Please, sir, I just
need to be transferred out of RSC.

19

20

A FEW GOOD MEN

(Lights up on JESSEP’s office.)
TOM. Excuse me, sir, Captain Markinson and Lt. Kendrick

to see you.
JESSEP. Thank you, Tom.
MARKINSON. Good morning, Colonel.
JESSEP. Matthew, Jon, have a seat.
MARKINSON. Thank you.
JESSEP. Ten-hundred hours, already hot enough to melt the
brass off your collar. I just had a Navy guy in here telling me we’re lucky. After all, it’s “dry heat.” Dry heat.
It’s a hundred and seven degrees outside, how am I
supposed to feel about that. Matthew, you’ve been
here the longest, is this about as hot as it gets or am I
actually trapped in hell.
MARKINSON. This is as hot as it’s been since maybe ’84,
Colonel.
JESSEP. ’84 was pretty bad?
MARKINSON. Got up to 119 degrees.
JESSEP. “Capering half in smoke, and half in fire.” (pause)
Moby Dick. (pause) Jon, when I quote Melville, you
don’t have to nod your head up and down like you
know what I’m talking about.
KENDRICK. Yes sir.
JESSEP. I’m not gonna regard you as less of a man because
you’re not well read.
KENDRICK. Thank you, sir.
JESSEP. I mean that Jon.
KENDRICK. I appreciate that, sir.
JESSEP. 119 degrees Fahrenheit.
MARKINSON. Yes sir.
JESSEP. You must’ve had Marines passing out right and left.
MARKINSON. No, the men were alright.
JESSEP. Nobody passed out?
MARKINSON. Not that I recall.

Hungry for More?
This is a Sample of the Script

Buy the full script and explore other titles

www.samuelfrench.com
www.samuelfrench-london.co.uk

Titles are subject to availability depending on your territory.

