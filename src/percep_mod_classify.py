import sys
import glob
import errno
from collections import defaultdict
import itertools
import codecs
#model = str(sys.argv[1])
f2=open('POSTEST','w',errors='ignore')
m = open("MODEL_GEN",'r',errors='ignore')
prob_dict={}
class_prob_dict={}
class_name_dict={}
words_weight={}
#read each line in the input file and store in a list 'input_words'
input_words=[]
class_count=0
j = 0
####### Determining Class Labels and Words Weight!

for line in m:
 if line.startswith("**__CLASSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS__**"):
  words=line.split()
  #print(words)
  class_name_dict[class_count]=words[1]
  #class_prob_dict[class_count]=words[2]
  words_weight[class_count]=defaultdict(int)
  class_count+=1
  line=next(m)
 l=line.split()
 #print (l)
 #print(l.length)
 words_weight[class_count-1][l[0]]=l[1]


#### Reading Input from Command line till 'quit' is typed!#################
"""sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore')
#a=list(iter(input,'quit'))
for line in sys.stdin:
 words=line.split()
 l=len(words)
 for i,w1 in enumerate(words):
  if i!=0 and i!=1:
   prevword=words[i-1]
   prevword2=words[i-2]
  elif i==0:
   prevword='BOS'
   prevword2='BOS'
  elif i==1:
   prevword=words[i-1]
   prevword2='BOS'
  if i != l-1 and i!= l-2:
   nextword=words[i+1]
   nextword2=words[i+2]
  elif i==l-1:
   nextword='EOS'
   nextword2='EOS'
  elif i==l-2:
   nextword=words[i+1]
   nextword2='EOS'       
  f2.write(str('l:'+prevword2+' '+'l:'+prevword+' ''c:'+w1+' '+'r:'+nextword+' '+'r:'+nextword2))
  f2.write('\n') 
f2.close()"""

sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore')
with open("MODEL_GEN", 'r') as f1:
 for line in f1:
  if line.startswith("**__CLASSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS__**"):
   words = line.split()
   class_name_dict[j] = words[1]
   words_weight[j] = defaultdict(int)
   j = j+1
   line = next(f1)
  words = line.split()
  words_weight[j-1][words[0]] = words[1]
for line in sys.stdin:
 if line != '\n':
  words=line.split()
  l=len(words)
  for i,w1 in enumerate(words):
   if i!=0 and i!=1:
    prevword=words[i-1]
    prevword2=words[i-2]
   elif i==0:
    prevword='BOS'
    prevword2='BOS'
   elif i==1:
    prevword=words[i-1]
    prevword2='BOS'
   if i != l-1 and i!= l-2:
    nextword=words[i+1]
    nextword2=words[i+2]
   elif i==l-1:
    nextword='EOS'
    nextword2='EOS'
   elif i==l-2:
    nextword=words[i+1]
    nextword2='EOS'       
   f2.write(str('l:'+prevword2+' '+'l:'+prevword+' '+'c:'+w1+' '+'r:'+nextword+' '+'r:'+nextword2))
   f2.write('\n') 
 elif line == '\n':
  f2.write('\n') 
f2.close()      




      
################# Tagging the Output! ###################################################3

"""
with open('POSTEST.txt','r') as f3:
 for line in f3:
  words=line.split()
  currword=words[2].split(':')
  weights1=defaultdict(int)
  if currword[1] in class_name_dict.values():
   for key1 in w:
    for word in words:
     weights1[label[key1]]+=int(w[key1][word])
"""

with open('POSTEST','r') as f3:
 for line in f3:
  if line!="\n":
   words=line.split()
   currword=words[2].split(':',1)
   weights=defaultdict(int)
   if currword[1] in class_name_dict.values():
    #print("Here")
    for i in words_weight:
     for w in words:
      weights[i]+=int(words_weight[i][w]) # automatically assigns zero to words not in the dictionary
    if currword[1]=='too' or currword[1]=='to':
     if(weights['too']>weights['to']):
      sys.stdout.write('too ')
     else:
      sys.stdout.write('to ')
    elif currword[1]=='Too' or currword[1]=='To':
     if(weights['Too']>weights['To']):
      sys.stdout.write('Too ')
       #print("Here-too")
     else:
      sys.stdout.write('To ')
       #print("Here-to")
    elif (currword[1]=='its' or currword[1]=="it's"):
     if(weights['its']>weights["it's"]):
      sys.stdout.write('its ')
     else:
      sys.stdout.write("it's ")
    elif (currword[1]=='Its' or currword[1]=="It's"):
     if(weights['Its']>weights["It's"]):
      sys.stdout.write('Its ')
     else:
      sys.stdout.write("It's ")
    elif (currword[1]=='your' or currword[1]=="you're"):
     if(weights["you're"]>weights["your"]):
      sys.stdout.write("you're ")
     else:
      sys.stdout.write("your ")
    elif (currword[1]=='Your' or currword[1]=="You're"):
     if(weights["You're"]>weights["Your"]):
      sys.stdout.write("You're ")
     else:
      sys.stdout.write("Your ")
    elif (currword[1]=='loose' or currword[1]=="lose"):
     if(weights["loose"]>weights["lose"]):
      sys.stdout.write("loose ")
     else:
      sys.stdout.write("lose ")
    elif (currword[1]=='Loose' or currword[1]=="Lose"):
     if(weights["Loose"]>weights["Lose"]):
      sys.stdout.write("Loose ")
     else:
      sys.stdout.write("Lose ")
    elif (currword[1]=='their' or currword[1]=="they're"):
     if(weights["they're"]>weights["their"]):
      sys.stdout.write("they're ")
     else:
      sys.stdout.write("their ")
    elif (currword[1]=='Their' or currword[1]=="They're"):
     if(weights["They're"]>weights["Their"]):
      sys.stdout.write("They're ")
     else:
      sys.stdout.write("Their ")
   else:
    sys.stdout.write(currword[1] + " " ) 
   if words[3]=='r:EOS': 
    sys.stdout.write('\n') 
  elif line=='\n':
   sys.stdout.write(line)  

   
     


  """index = max(weights,key=weights.get)
  sys.stdout.write(currword[1]+'/'+class_name_dict[index]+' ') 
  if words[2]=='NEXT:ee': 
   sys.stdout.write('\n') """
